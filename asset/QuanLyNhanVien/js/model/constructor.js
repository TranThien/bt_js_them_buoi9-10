function Staff(
  _account,
  _name,
  _email,
  _pass,
  _date,
  _salary,
  _position,
  _time
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.pass = _pass;
  this.date = _date;
  this.salary = _salary;
  this.position = _position;
  this.time = _time;
  this.SumSalary = function () {
    var result = 0;
    if (this.position == "Sếp") {
      result += this.salary * 1 * 3;
    } else if (this.position == "Trưởng phòng") {
      result += this.salary * 1 * 2;
    } else if (this.position == "Nhân viên") {
      result += this.salary * 1;
    } else {
      return;
    }
    result = new Intl.NumberFormat().format(result);
    return `${result} VND`;
  };
  this.Type = function () {
    var result = "";
    if (this.time >= 192) {
      result += "Xuất sắc";
    } else if (this.time >= 176 && this.time < 192) {
      result += " Giỏi";
    } else if (this.time >= 160 && this.time < 176) {
      result += " Khá";
    } else {
      result += " Trung bình";
    }
    return result;
  };
}
