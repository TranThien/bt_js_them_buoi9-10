function getInformations() {
  var account = document.getElementById("tknv").value.trim();
  var name = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var passWord = document.getElementById("password").value.trim();
  var date = document.getElementById("datepicker").value;
  var salary = document.getElementById("luongCB").value.trim();
  var position = document.getElementById("chucvu").value;
  var timeWorking = document.getElementById("gioLam").value.trim();
  var staff = new Staff(
    account,
    name,
    email,
    passWord,
    date,
    salary,
    position,
    timeWorking
  );

  return staff;
}

function renderStaff(list) {
  // var content = list.map(function (staff) {
  //   var content = `<tr>
  //         <td>${staff.account}</td>
  //         <td>${staff.name}</td>
  //         <td>${staff.email}</td>
  //         <td>${staff.date}</td>
  //         <td>${staff.position}</td>
  //         <td>${staff.SumSalary()}</td>
  //         <td>${staff.Type()}</td>
  //         <td>
  //         <button onclick="deleteStaff('${
  //           staff.account
  //         }')" class="btn btn-danger">Xóa</button>
  //         <button onclick="fixStaff('${
  //           staff.account
  //         }')" class="btn btn-primary fix-js">Sửa</button>
  //         </td>
  //         </tr>`;
  // });
  // contentHTML += content;
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var staff = list[i];
    var content = `
     <tr>
           <td>${staff.account}</td>
           <td>${staff.name}</td>
           <td>${staff.email}</td>
           <td>${staff.date}</td>
           <td>${staff.position}</td>
           <td>${staff.SumSalary()}</td>
           <td>${staff.Type()}</td>
           <td>
           <button onclick="deleteStaff('${
             staff.account
           }')" class="btn btn-danger">Xóa</button>
           <button onclick="fixStaff('${
             staff.account
           }')" data-toggle="modal" data-target="#myModal" class="btn btn-primary fix-js">Sửa</button>
           </td>
           </tr>`;
    contentHTML += content;
  }

  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function showInformations(staff) {
  document.getElementById("tknv").value = staff.account;
  document.getElementById("name").value = staff.name;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.pass;
  document.getElementById("datepicker").value = staff.date;
  document.getElementById("luongCB").value = staff.salary;
  document.getElementById("chucvu").value = staff.position;
  document.getElementById("gioLam").value = staff.time;
}

function resetForm() {
  document.getElementById("myForm").reset();
}
