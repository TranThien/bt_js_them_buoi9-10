// Kiểm tra rỗng
function checkEmpty(value, errorMessage) {
  if (value.length == 0) {
    document.getElementById(errorMessage).innerHTML =
      "Vui lòng nhập trường này";
    document.getElementById(errorMessage).style.display = "block";
    return false;
  } else {
    document.getElementById(errorMessage).style.display = "none";
    document.getElementById(errorMessage).innerHTML = "";
    return true;
  }
}
// Kiểm tra tài khoản
function checkAccount(value, errorMessage, minLength, maxLength) {
  if (value.length >= minLength && value.length < maxLength) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";

    return true;
  } else {
    document.getElementById(errorMessage).style.display = "block";

    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng nhập tối đa từ ${minLength} - ${maxLength} ký số`;
    return false;
  }
}
// Kiểm tra số
function checkNumber(value, errorMessage) {
  var regex = /^[0-9]+$/;

  if (value.match(regex)) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML = "Trường này phải là số";
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// Kiểm tra tên
function checkName(value, errorMessage) {
  // var regexText = new RegExp("^[A-Za-z]+$");

  var regexText = new RegExp("[A-zÀ-ÿ]");
  if (regexText.test(value)) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML = "Trường này phải là chữ";
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// Kiểm tra Email
function checkEmail(value, errorMessage) {
  var re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (re.test(value)) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML =
      "Trường này phải là Email";
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// Kiểm tra passWord
function checkPassword(value, errorMessage) {
  var regularExpression = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9_])"
  );
  var result = regularExpression.test(value);
  if (result) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng nhập từ tối thiểu 6 - 10 ký tự bao gồm ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// Kiểm tra độ dài pass
function checkLengthPassword(value, errorMessage, minLength, maxLength) {
  if (value.length >= minLength && value.length <= maxLength) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng nhập từ ${minLength} - ${maxLength} ký tự bao gồm ít nhất 1 ký tự số, 1 ký tự in hoa và 1 ký tự đặc biệt`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// Kiểm tra ngày

// function checkDate(value, errorMessage) {
//   var re = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
//   if (re.test(value)) {
//     document.getElementById(errorMessage).innerHTML = "";
//     document.getElementById(errorMessage).style.display = "none";
//     return true;
//   } else {
//     document.getElementById(
//       errorMessage
//     ).innerHTML = `Vui lòng nhập trường này`;
//     document.getElementById(errorMessage).style.display = "block";
//     return false;
//   }
// }
// kiểm tra tiền lương cơ bản
function checkSaraly(value, errorMessage, min, max) {
  var result = value * 1;

  if (result * 1 >= min && result * 1 <= max) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng nhập số tiền trong khoảng từ 1.000.000VNĐ - 20.000.000VNĐ`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
// kiểm tra lựa chọn chức vụ
function checkOption(value, errorMessage) {
  if (value == "Chọn chức vụ") {
    document.getElementById(
      errorMessage
    ).innerHTML = `Vui lòng chọn trường này`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  } else {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  }
}
// kiểm tra thời gian làm việc
function checkTimeWorking(value, errorMessage, min, max) {
  if (value >= min && value <= max) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(
      errorMessage
    ).innerHTML = `Số giờ làm từ ${min} - ${max} giờ . Bạn vui lòng nhập lại`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}

// Kiểm tra trùng lặp account or id
function duplicateCheck(account, errorMessage, listStaff) {
  var index = listStaff.findIndex(function (staff) {
    return staff.account == account;
  });
  if (index == -1) {
    document.getElementById(errorMessage).innerHTML = "";
    document.getElementById(errorMessage).style.display = "none";
    return true;
  } else {
    document.getElementById(errorMessage).innerHTML = `Tài khoản đã tồn tại`;
    document.getElementById(errorMessage).style.display = "block";
    return false;
  }
}
