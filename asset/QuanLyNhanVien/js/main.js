var listStaff = [];
// lấy dữ liệu lên từ local
var dataJson = localStorage.getItem("listStaff");
if (dataJson) {
  // convert sau khi lấy dữ liệu lên chuyển đổi json thành js
  var dataRaw = JSON.parse(dataJson);

  listStaff = dataRaw.map(function (item) {
    var staff = new Staff(
      item.account,
      item.name,
      item.email,
      item.pass,
      item.date,
      item.salary,
      item.position,
      item.time
    );
    return staff;
  });
  renderStaff(listStaff);
}

function saveLocalStorage() {
  // convert js thành json
  var listStaffJson = JSON.stringify(listStaff);
  // lưu xuống local
  localStorage.setItem("listStaff", listStaffJson);
}

function addStaff() {
  var newStaff = getInformations();

  var isValid = true;
  isValid =
    isValid & checkEmpty(newStaff.account, "tbTKNV") &&
    checkAccount(newStaff.account, "tbTKNV", 4, 6) &&
    checkNumber(newStaff.account, "tbTKNV") &&
    duplicateCheck(newStaff.account, "tbTKNV", listStaff);
  isValid =
    isValid & checkEmpty(newStaff.name, "tbTen") &&
    checkName(newStaff.name, "tbTen");

  isValid =
    isValid & checkEmpty(newStaff.email, "tbEmail") &&
    checkEmail(newStaff.email, "tbEmail");
  isValid =
    isValid & checkEmpty(newStaff.pass, "tbMatKhau") &&
    checkPassword(newStaff.pass, "tbMatKhau") &&
    checkLengthPassword(newStaff.pass, "tbMatKhau", 6, 10);
  isValid = isValid & checkEmpty(newStaff.date, "tbNgay");
  isValid =
    isValid & checkEmpty(newStaff.salary, "tbLuongCB") &&
    checkNumber(newStaff.salary, "tbLuongCB") &&
    checkSaraly(newStaff.salary, "tbLuongCB", 1000000, 20000000);
  isValid =
    isValid & checkEmpty(newStaff.position, "tbChucVu") &&
    checkOption(newStaff.position, "tbChucVu");
  isValid =
    isValid & checkEmpty(newStaff.time, "tbGiolam") &&
    checkNumber(newStaff.time, "tbGiolam") &&
    checkTimeWorking(newStaff.time, "tbGiolam", 80, 200);
  if (!isValid) return;

  listStaff.push(newStaff);
  saveLocalStorage();
  renderStaff(listStaff);
  resetForm();
}

function deleteStaff(account) {
  var index = listStaff.findIndex(function (staff) {
    return staff.account == account;
  });

  if (index == -1) {
    return;
  }
  listStaff.splice(index, 1);
  saveLocalStorage();
  renderStaff(listStaff);
}
function fixStaff(account) {
  var index = listStaff.findIndex(function (staff) {
    return staff.account == account;
  });
  if (index == -1) {
    return;
  }
  // Đặt biến và dùng tên biến này show thông tin lên form

  var staff = listStaff[index];
  showInformations(staff);
  document.getElementById("tknv").disabled = true;
}
function UpdateStaff() {
  var openModal = document.querySelector(".modal.fade");
  var editStaff = getInformations();
  console.log(editStaff.account);
  var index = listStaff.findIndex(function (staff) {
    return staff.account == editStaff.account;
  });
  console.log(index);
  if (index == -1) {
    return;
  }
  listStaff[index] = editStaff;
  saveLocalStorage();
  renderStaff(listStaff);
  resetForm();
  document.getElementById("tknv").disabled = false;
}

function searchType() {
  var search = document.querySelector("#searchName").value;

  var newListStaff = listStaff.filter(function (staff) {
    return staff.Type().toUpperCase().includes(search.toUpperCase());
  });
  renderStaff(newListStaff);
}
